package com.time.store.setting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.setting.entity.po.SettingOrder;
import com.time.store.setting.entity.po.SettingPay;

/**
 * 支付设置mapper
 */
public interface SettingPayMapper extends BaseMapper<SettingPay> {

}
