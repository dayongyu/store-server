package com.time.store.setting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.setting.entity.po.SettingProduct;
import com.time.store.setting.entity.po.SettingStore;

/**
 * 店铺设置mapper
 */
public interface SettingStoreMapper extends BaseMapper<SettingStore> {

}
