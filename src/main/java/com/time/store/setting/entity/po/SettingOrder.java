package com.time.store.setting.entity.po;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 订单设置表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("setting_order")
@Data
public class SettingOrder extends Model<SettingOrder> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 实名验证是否校验次数 (1: 不限制 2:限制)
     */
    private int realNameVerificationCheckTimesEnable;

    /**
     * 实名验证校验次数
     */
    private int realNameVerificationCheckTimes;

    /**
     * 默认库存扣减方式 (1: 拍下减库存 2:付款减库存)
     */
    private int defaultInventoryDeductionMethod;

    /**
     * 付款减库存预占时间 单位:分钟
     */
    private Integer paymentMinusInventoryPreemptionTime;

    /**
     * 超卖订单处理 (1:人工处理 2:系统自动退款)
     */
    private int oversoldOrderProcessing;

    /**
     * 待付款订单取消时间 单位:分钟
     */
    private Integer cancellationTimeOfPendingOrder;

    /**
     * 待付款订单催付弹窗(1:开启 2:关闭)
     */
    private int pendingPaymentOrderReminderPopUpWindow;

    /**
     * 发货后自动确认收货时间 单位:天
     */
    private Integer confirmTheDeliveryTimeAfterDelivery;

    /**
     * 未付款订单改价维度 (1:在整单基础上改价 2:在单商品货款基础上改价)
     */
    private int unpaidOrderChangePriceDimension;

    /**
     * 单品多数量发货 (1:开启 2:关闭)
     */
    private int singleProductShippedInLargeQuantities;

    /**
     * 海外订单下单 (1:开启 2:关闭)
     */
    private int placeAnOverseasOrder;

    /**
     * 收货人姓名校验 (1:开启 2:关闭)
     */
    private int consigneeNameVerification;

    /**
     * 自动同意退货申请 (1:开启 2:关闭)
     */
    private int automaticallyAgreeToReturnApplication;

    /**
     * 买家退货后商家自动确认收货时间 单位:天
     */
    private Integer confirmReceiptAfterReturnTime;

    /**
     * 买家开发票设置： (1:开启 2:关闭)
     */
    private int buyerInvoicingSettings;

    /**
     * 下单模式： (1:普通下单 2:商详页极速下单 3:分不极速下单)
     */
    private int orderMode;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
