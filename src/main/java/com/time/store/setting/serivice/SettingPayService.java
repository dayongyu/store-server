package com.time.store.setting.serivice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.setting.entity.po.SettingOrder;
import com.time.store.setting.entity.po.SettingPay;
import com.time.store.setting.mapper.SettingOrderMapper;
import com.time.store.setting.mapper.SettingPayMapper;
import org.springframework.stereotype.Service;

@Service
public class SettingPayService extends ServiceImpl<SettingPayMapper, SettingPay> implements IService<SettingPay> {
}
