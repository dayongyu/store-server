package com.time.store.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.task.entity.bo.TaskStore;
import com.time.store.task.entity.bo.TaskTutorials;

public interface TaskTutorialsMapper extends BaseMapper<TaskTutorials> {
}
