package com.time.store.task.controller;


import com.time.store.task.entity.dto.TaskInfoDTO;
import com.time.store.task.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 任务
 *@description: 任务
 *@menu 任务
 */
@RestController
@RequestMapping("/api/task")
@Slf4j
public class TaskController {

    @Autowired
    private TaskService taskService;



    /**
     * 获取当前店铺任务
     * @status done
     * @return
     */
    @GetMapping("/getCurrent")
    public TaskInfoDTO getCurrent(){
        return taskService.getCurrent();
    }
}
