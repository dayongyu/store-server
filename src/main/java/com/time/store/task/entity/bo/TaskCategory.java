package com.time.store.task.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 任务类别
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("task_category")
@Data
public class TaskCategory extends Model<TaskCategory> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 任务类别标题
     */
    private String name;

    /**
     * 任务类别权重
     */
    private Integer weight;

    /**
     * 任务描述
     */
    private String description;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
