package com.time.store.expand.controller;


import com.time.store.auth.entity.bo.Modules;
import com.time.store.auth.service.ModulesService;
import com.time.store.core.annotations.IgnoreAuth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 扩展控制器
 *@description: 扩展
 *@menu 扩展
 */
@RestController
@RequestMapping("/api/expland/")
@Slf4j
public class ExpandController {
    @Autowired
    private ModulesService modulesService;
    /**
     * 修改菜单信息
     * @status done
     * @return
     */
    @PutMapping("/updateModule/{id}")
    @IgnoreAuth
    public void  updateModule(@RequestBody Modules modules,@PathVariable("id") Integer id){
        modules.setId(id);
        modulesService.updateById(modules);
    }
}
