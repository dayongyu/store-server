package com.time.store.job;

import cn.hutool.core.lang.Console;
import com.time.store.core.util.AuthUtil;
import com.time.store.order.entity.bo.OrderDayStatistical;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.service.OrderDayStaticalService;
import com.time.store.order.service.OrderInfoService;
import com.time.store.order.service.OrderStaticalService;
import com.time.store.store.entity.bo.StoreInfo;
import com.time.store.store.service.StoreInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;


@Component
public class OrderStatisticalJob{

    @Autowired
    private OrderStaticalService orderStaticalService;

    @Autowired
    private StoreInfoService storeInfoService;

    @Autowired
    private OrderInfoService orderInfoService;

    @Autowired
    private OrderDayStaticalService orderDayStaticalService;

    /**
     * 订单每日统计
     */
    @Scheduled(cron = "0 0 0 * * ?")
    private void orderStatisticalTask(){
        // 获取所有店铺
        List<StoreInfo> storeInfos = storeInfoService.list();

        for (StoreInfo storeInfo : storeInfos) {
            // 订单总量统计
            AuthUtil.setRequestStoreId(storeInfo.getId());
            OrderStatistical newOrderStatistical = orderInfoService.getBaseMapper().orderStatistical();
            newOrderStatistical.setStoreId(storeInfo.getId());
            OrderStatistical orderStatistical = orderStaticalService.getBaseMapper().getByStoreId();
            orderStatistical.setPendingPaymentOrderNum(newOrderStatistical.getPendingPaymentOrderNum());
            orderStatistical.setRightsProtectionOrderNum(newOrderStatistical.getRightsProtectionOrderNum());
            orderStatistical.setToBeDeliveredOrderNum(newOrderStatistical.getToBeDeliveredOrderNum());
            orderStaticalService.updateById(orderStatistical);

            // 订单日统计
            OrderDayStatistical orderDayStatistical = orderInfoService.getBaseMapper().orderStatisticalDay(LocalDate.now());
            orderDayStatistical.setStoreId(storeInfo.getId());
            orderDayStaticalService.save(orderDayStatistical);
        }


    }
}
