package com.time.store.news.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 新闻信息实体
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("news_info")
@Data
public class NewsInfo extends Model<NewsInfo> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 新闻名称
     */
    private String title;

    /**
     * 分类权重
     */
    private Integer weight;

    /**
     * 内容
     */
    private String body;

    /**
     * 跳转url
     */
    private String linkUrl;

    /**
     * 新闻类别id
     */
    private Integer newsCategoryId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
