package com.time.store.news.consts;

public enum NewsPositionEnum {
    HOME_RIGHT("HOME_RIGHT","首页右"),
    FUNCTIONALLY_NEW("FUNCTIONALLY_NEW","功能上新")
    ;
    private String key;
    private String name;

    NewsPositionEnum(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
