package com.time.store.system.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 系统_垃圾表
 */
@EqualsAndHashCode(callSuper = true)
@TableName
@Data
public class SystemGarbage extends Model<SystemGarbage> {
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 记录
     */
    @TableField(value = "`rows`")
    private String rows;
    /**
     * 操作人
     */
    private Integer operatingUserId;
    /**
     * 删除sql
     */
    private String deleteSql;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
