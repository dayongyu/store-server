package com.time.store.core.exception;

import cn.hutool.core.text.StrFormatter;
import lombok.Data;
import org.slf4j.MDC;

@Data
public class BizException extends RuntimeException{
    private String requestId;

    public BizException(String errorMessage){
        super(errorMessage);
        setRequestId(MDC.get("requestId"));
    }

    public BizException(String errorMessageTemplate,Object... objects){
        super( StrFormatter.format(errorMessageTemplate, objects));
        setRequestId(MDC.get("requestId"));
    }
}
