package com.time.store.core.util;


import com.time.store.auth.entity.base.ActiveUser;
import com.time.store.core.consts.RequestAttributeKeyConst;
import com.time.store.store.entity.bo.StoreInfo;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class AuthUtil {

    private final static ThreadLocal<Map<String, Object>> holder = new ThreadLocal<>();

    /**
     * 获取当前用户
     */
    public static ActiveUser getCurrUser() {
        HttpServletRequest request = ContextUtil.getRequest();
        ActiveUser competenceUser = (ActiveUser) request.getAttribute(RequestAttributeKeyConst.USERINFO);
        return competenceUser;
    }

    /**
     * 获取当前店铺
     */
    public static StoreInfo getCurrStore() {
        HttpServletRequest request = ContextUtil.getRequest();
        ActiveUser currUser = (ActiveUser) request.getAttribute(RequestAttributeKeyConst.USERINFO);
        if (currUser == null)
            return null;
        return currUser.getStoreInfo();
    }


    /**
     * 设置店铺id
     */
    public static void setRequestStoreId(Integer storeId) {
        if (RequestContextHolder.getRequestAttributes() != null) {
            HttpServletRequest request = ContextUtil.getRequest();
            request.setAttribute(RequestAttributeKeyConst.STOREID, storeId);
        }
        setStoreId(storeId);
    }

    public static void setStoreId(Object value) {
        getContext().put(RequestAttributeKeyConst.STOREID, value);
    }

    public static Integer getStoreId() {
        return (Integer) getContext().get(RequestAttributeKeyConst.STOREID);
    }

    private static Map<String, Object> getContext() {
        Map map = holder.get();
        if (map == null) {
            map = new HashMap();
            holder.set(map);
        }
        return map;
    }


    /**
     * 获取店铺id
     */
    public static Integer getRequestStoreId() {
        if (RequestContextHolder.getRequestAttributes() != null) {
            HttpServletRequest request = ContextUtil.getRequest();
            return (Integer) request.getAttribute(RequestAttributeKeyConst.STOREID);
        }
        return getStoreId();
    }

    /**
     * 获取当前店铺Id
     */
    public static Integer getCurrStoreId() {
        HttpServletRequest request = ContextUtil.getRequest();
        ActiveUser currUser = (ActiveUser) request.getAttribute(RequestAttributeKeyConst.USERINFO);
        if (currUser == null)
            return null;
        return currUser.getStoreInfo().getId();
    }

    /**
     * 获取当前用户id
     *
     * @return
     */
    public static Integer getCurrUserId() {
        ActiveUser currUser = getCurrUser();
        if (currUser == null)
            return null;
        return currUser.getActiveUsers().getId();
    }


    /**
     * 获取当前用户登录名
     *
     * @return
     */
    public static String getCurrUserLoginName() {
        ActiveUser currUser = getCurrUser();
        if (currUser == null)
            return null;
        return currUser.getActiveUsers().getLoginName();
    }

    public static String getToken() {
        HttpServletRequest request = ContextUtil.getRequest();
        return getToken(request);
    }

    public static String getToken(HttpServletRequest request) {
        String token = request.getParameter("token");
        if (token == null)
            token = request.getHeader("token");
        return token;
    }


}
