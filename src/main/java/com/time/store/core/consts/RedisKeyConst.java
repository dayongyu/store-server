package com.time.store.core.consts;


import java.time.Duration;

/**
 * redis key
 */
public enum RedisKeyConst {
    /**
     * 权限缓存时间由配置文件设置
     */
    AUTH(null),
    IMG_CODE(Duration.ofMinutes(2)),
    /**
     * 刷新权限缓存时间由配置文件设置
     */
    REFRESH(null),
    ;


    private Duration duration;

    RedisKeyConst(Duration duration) {
        this.duration = duration;
    }


    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
