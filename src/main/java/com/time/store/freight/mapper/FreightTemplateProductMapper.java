package com.time.store.freight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.freight.entity.bo.FreightDeliveryArea;
import com.time.store.freight.entity.bo.FreightTemplateProduct;

/**
 * 运费模板商品关联mapper
 */
public interface FreightTemplateProductMapper extends BaseMapper<FreightTemplateProduct> {

}
