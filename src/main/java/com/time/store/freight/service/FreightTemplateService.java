package com.time.store.freight.service;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.core.entity.base.BasePageRequest;
import com.time.store.core.util.AssertUtil;
import com.time.store.core.util.AuthUtil;
import com.time.store.freight.entity.bo.FreightDeliveryArea;
import com.time.store.freight.entity.bo.FreightTemplate;
import com.time.store.freight.entity.dto.CreateFreightTemplateAreaDTO;
import com.time.store.freight.entity.dto.CreateFreightTemplateDTO;
import com.time.store.freight.entity.vo.CreateFreightTemplateVO;
import com.time.store.freight.mapper.FreightTemplateMapper;
import com.time.store.order.entity.bo.OrderAfterSale;
import com.time.store.order.entity.dto.AfterSalePageQuery;
import com.time.store.order.entity.dto.OrderAfterSaleExportDTO;
import com.time.store.order.entity.dto.ReimburseDTO;
import com.time.store.order.mapper.OrderAfterSaleMapper;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FreightTemplateService extends ServiceImpl<FreightTemplateMapper, FreightTemplate> implements IService<FreightTemplate> {
    /**
     * 模板区域service
     */
    @Autowired
    private FreightDeliveryAreaService freightDeliveryAreaService;

    /**
     * 保存运费模板
     *
     * @param createFreightTemplateDTO
     */
    public void createTemplate(CreateFreightTemplateDTO createFreightTemplateDTO) {
        // 保存运费模板
        FreightTemplate freightTemplate = new FreightTemplate();
        BeanUtils.copyProperties(createFreightTemplateDTO, freightTemplate);
        freightTemplate.setStoreId(AuthUtil.getCurrStoreId());
        save(freightTemplate);
        // 保存运费模板区域
        saveTemplateArea(createFreightTemplateDTO.getTemplateArea());
    }

    private void saveTemplateArea(List<CreateFreightTemplateAreaDTO> createFreightTemplateAreaDTOS) {
        if (CollectionUtil.isNotEmpty(createFreightTemplateAreaDTOS)) {
            List<FreightDeliveryArea> freightDeliveryAreaList = createFreightTemplateAreaDTOS.stream().map(item -> {
                FreightDeliveryArea freightDeliveryArea = new FreightDeliveryArea();
                BeanUtils.copyProperties(item, freightDeliveryArea);
                freightDeliveryArea.setStoreId(AuthUtil.getCurrStoreId());
                return freightDeliveryArea;
            }).collect(Collectors.toList());
            freightDeliveryAreaService.saveBatch(freightDeliveryAreaList);
        }
    }

    public Page<CreateFreightTemplateVO> pageTemplate(BasePageRequest basePageRequest) {
        // 总条数
        int total = baseMapper.pageCount();
        Page<CreateFreightTemplateVO> createFreightTemplateVOPage = new Page<>(basePageRequest.getCurrent(), basePageRequest.getSize(), total, false);
        basePageRequest.setCurrent((basePageRequest.getCurrent() - 1) * basePageRequest.getSize());

        List<Integer> templateIds = baseMapper.pageTemplateIds(basePageRequest);
        if (CollectionUtil.isEmpty(templateIds)) {
            return createFreightTemplateVOPage;
        }
        List<CreateFreightTemplateVO> createFreightTemplateVOList = baseMapper.pageTemplate(templateIds);
        createFreightTemplateVOPage.setRecords(createFreightTemplateVOList);
        return createFreightTemplateVOPage;
    }

    /**
     * 修改模板
     *
     * @param id
     * @param updateFreightTemplateDTO
     */
    public void updateTemplate(Integer id, CreateFreightTemplateDTO updateFreightTemplateDTO) {
        /**
         * 模板id
         */
        FreightTemplate freightTemplate = getById(id);
        AssertUtil.notNull(freightTemplate, "运费模板未找到");
        BeanUtils.copyProperties(updateFreightTemplateDTO,freightTemplate);
        updateById(freightTemplate);


        // 保存运费模板区域
        LambdaQueryChainWrapper<FreightDeliveryArea> deleteAreaWrapper = freightDeliveryAreaService.lambdaQuery().eq(FreightDeliveryArea::getFreightTemplateId, freightTemplate.getId());
        freightDeliveryAreaService.remove(deleteAreaWrapper);
        saveTemplateArea(updateFreightTemplateDTO.getTemplateArea());
    }
}
