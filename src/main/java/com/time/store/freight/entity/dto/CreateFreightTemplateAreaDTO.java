package com.time.store.freight.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 创建运费模板
 */
@Data
public class CreateFreightTemplateAreaDTO {
    /**
     * 可配送区域code
     */
    @NotEmpty
    private String deliverableAreaCode;

    /**
     * 可配送区域name
     */
    @NotEmpty
    private String deliverableAreaName;

    /**
     * 首个/首重
     */
    @NotNull
    private Integer firstItem;

    /**
     * 首个/首重 运费
     */
    @NotNull
    private Integer firstAmount;

    /**
     * 续件/续重
     */
    @NotNull
    private Integer continuousItem;

    /**
     * 续件/续重 运费
     */
    @NotNull
    private Integer continuousAmount;
}
