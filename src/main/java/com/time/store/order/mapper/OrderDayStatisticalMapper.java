package com.time.store.order.mapper;
import java.util.List;
import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.order.entity.bo.OrderDayStatistical;

import java.time.LocalDate;

/**
 * 订单昨日统计mapper
 */
public interface OrderDayStatisticalMapper extends BaseMapper<OrderDayStatistical> {
    /**
     * 查询指定日期订单统计信息
     *
     * @return
     */
    OrderDayStatistical selectByDate(@Param("date") LocalDate date);


    /**
     * 查询指定日期区间订单统计信息
     * @param dateStart
     * @param dateEnd
     * @return
     */
    List<OrderDayStatistical> selectByDateBetween(@Param("dateStart") LocalDate dateStart,@Param("dateEnd") LocalDate dateEnd);
}
