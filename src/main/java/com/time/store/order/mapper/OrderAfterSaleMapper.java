package com.time.store.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.order.entity.bo.OrderAfterSale;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.entity.dto.AfterSalePageQuery;
import com.time.store.order.entity.dto.OrderAfterSaleExportDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单售后mapper
 */
public interface OrderAfterSaleMapper extends BaseMapper<OrderAfterSale> {

    /**
     * 分页查询
     *
     * @param afterSalePageQuery
     * @param page
     * @return
     */
    Page<OrderAfterSale> pageAfterSale(@Param("afterSalePageQuery") AfterSalePageQuery afterSalePageQuery, @Param("page") Page<OrderAfterSale> page);

    /**
     * 查询售后
     * @param afterSalePageQuery
     * @return
     */
    List<OrderAfterSaleExportDTO> afterSale(@Param("afterSalePageQuery") AfterSalePageQuery afterSalePageQuery);
}
