package com.time.store.order.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.order.entity.bo.OrderShipment;
import com.time.store.order.entity.dto.OrderExportShipmentDto;
import com.time.store.order.entity.dto.OrderImportShipmentDto;
import com.time.store.order.entity.dto.OrderShipmentDTO;
import com.time.store.order.entity.dto.OrderShipmentPageReq;
import com.time.store.order.service.OrderShipmentService;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 订单发货信息控制器
 *
 * @description: 订单发货信息
 * @menu 订单发货
 */
@RestController
@RequestMapping("/api/order/shipment")
public class OrderShipmentController {
    @Autowired
    public OrderShipmentService orderShipmentService;

    /**
     * 分页查询发货信息
     *
     * @param orderShipmentPageReq
     * @return
     * @status done
     */
    @GetMapping("page")
    public Page<OrderShipment> page(@Validated OrderShipmentPageReq orderShipmentPageReq) {
        return orderShipmentService.pageOrderShipment(orderShipmentPageReq);
    }

    /**
     * 单个发货
     *
     * @param orderShipmentDto
     * @return
     * @status done
     */
    @PostMapping("")
    public OrderShipment orderShipment(@RequestBody @Validated OrderShipmentDTO orderShipmentDto) {
        return orderShipmentService.ship(orderShipmentDto, 1);
    }

    /**
     * 批量发货模板下载
     */
    @GetMapping("downBatchTemplate")
    public void downBatchTemplate(HttpServletResponse response) throws IOException {
        Workbook workbook = orderShipmentService.downBatchTemplate(response);
        workbook.write(response.getOutputStream());
    }

    /**
     * 发货订单批量导入
     *
     * @param file
     */
    @PostMapping("/importBatchShipment")
    public void importBatchShipment(@RequestParam("file") MultipartFile file) throws Exception {
        orderShipmentService.importBatchShipment(file);
    }
}
