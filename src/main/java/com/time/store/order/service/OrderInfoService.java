package com.time.store.order.service;


import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.core.util.NoCardUtil;
import com.time.store.order.entity.bo.OrderInfo;
import com.time.store.order.entity.dto.*;
import com.time.store.order.entity.vo.OrderProductResp;
import com.time.store.order.mapper.OrderInfoMapper;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class OrderInfoService extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IService<OrderInfo> {

    /**
     * 分页查询订单
     *
     * @param orderPageQuery
     * @return
     */
    public Page<OrderProductResp> pageOrder(OrderPageQuery orderPageQuery) {
        Page<OrderProductResp> page = new Page<>();
        page.setCurrent(orderPageQuery.getCurrent());
        page.setSize(orderPageQuery.getSize());
        return baseMapper.pageOrder(orderPageQuery, page);
    }


    public Workbook exportOrder(OrderPageExportReq orderPageExportReq, HttpServletResponse response) {
        switch (orderPageExportReq.getExportType()) {
            case 1:
                // 标准报表
                return exportOrderStandardDimension(orderPageExportReq, response);
            case 2:
                // 订单维度报表
                return exportOrderDimension(orderPageExportReq, response);
            case 3:
                // 商品维度报表
                return exportOrderProductDimension(orderPageExportReq, response);
        }


        return null;
    }

    private Workbook exportOrderStandardDimension(OrderPageExportReq orderPageExportReq, HttpServletResponse response) {
        ExportParams params = new ExportParams("订单标准维度数据", "订单");
        String fileName = URLEncoder.encode("订单标准维度数据", StandardCharsets.UTF_8);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xls\"");
        List<OrderStandardExportDTO> orderStandardExportDTOS = baseMapper.exportOrderStandard(orderPageExportReq);
        Workbook workbook = ExcelExportUtil.exportExcel(params, OrderStandardExportDTO.class, orderStandardExportDTOS);
        return workbook;
    }

    private Workbook exportOrderDimension(OrderPageExportReq orderPageExportReq, HttpServletResponse response) {
        ExportParams params = new ExportParams("订单维度数据", "订单");
        String fileName = URLEncoder.encode("订单维度数据", StandardCharsets.UTF_8);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xls\"");
        List<OrderExportDTO> orderExportDTOS = baseMapper.exportOrder(orderPageExportReq);
        Workbook workbook = ExcelExportUtil.exportExcel(params, OrderExportDTO.class, orderExportDTOS);
        return workbook;
    }

    private Workbook exportOrderProductDimension(OrderPageExportReq orderPageExportReq, HttpServletResponse response) {
        ExportParams params = new ExportParams("订单商品维度数据", "订单");
        String fileName = URLEncoder.encode("订单商品维度数据", StandardCharsets.UTF_8);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xls\"");
        List<OrderProductExportDTO> orderProductExportDTOS = baseMapper.exportOrderProduct(orderPageExportReq);
        Workbook workbook = ExcelExportUtil.exportExcel(params, OrderProductExportDTO.class, orderProductExportDTOS);
        return workbook;
    }

    public void star(OrderStarDTO orderStarDTO) {
        lambdaUpdate()
                .set(OrderInfo::getPayStarred, orderStarDTO.getPayStarred())
                .eq(OrderInfo::getId, orderStarDTO.getOrderId())
                .update();
    }

    public String generateOrderNo() {

        String orderNo = null;
        do {
            orderNo = NoCardUtil.generateNo("ON");
            // 查询数据库是否已有单号
            Integer shippingCodeNoCount = lambdaQuery().eq(OrderInfo::getOrderNo, orderNo).count();
            if (shippingCodeNoCount > 0) {
                orderNo = null;
            }
        } while (orderNo == null);


        return orderNo;
    }

    public String generateExternalPayNo() {

        String externalPayNo = null;
        do {
             externalPayNo = NoCardUtil.generateNo("EP");

            // 查询数据库是否已有单号
            Integer shippingCodeNoCount = lambdaQuery().eq(OrderInfo::getExternalPayNo, externalPayNo).count();
            if (shippingCodeNoCount > 0) {
                externalPayNo = null;
            }
        } while (externalPayNo == null);


        return externalPayNo;
    }
}
