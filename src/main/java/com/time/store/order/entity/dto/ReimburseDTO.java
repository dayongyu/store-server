package com.time.store.order.entity.dto;

import lombok.Data;
import org.jeecgframework.poi.excel.annotation.ExcelIgnore;

import javax.validation.constraints.NotNull;

@Data
public class ReimburseDTO {
    /**
     * 售后id
     */
    @NotNull
    private Integer id;
    /**
     * 是否同意
     */
    @NotNull
    private Boolean isAgree;

    /**
     * 收货人省
     */
    private String receiverProvince;

    /**
     * 收货人市
     */
    private String receiverCity;

    /**
     * 收货人区
     */
    private String receiverArea;

    /**
     * 收货人详细地址
     */
    private String receiverAddress;
}
