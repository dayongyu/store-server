package com.time.store.order.entity.dto;


import com.time.store.core.consts.DateTimeFormatConst;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import org.jeecgframework.poi.excel.annotation.ExcelIgnore;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Data
@ExcelTarget("orderProductExportDTO")
public class OrderProductExportDTO {
    /**
     * 订单id
     */
    @ExcelIgnore
    private Integer orderId;
    /**
     * 订单编号
     */
    @Excel(name = "订单编号")
    private String orderNo;
    /**
     * 外部支付编号
     */
    @Excel(name = "外部支付编号")
    private String externalPayNo;

    /**
     * 订单状态(1:待付款 2:待发货 3:已发货 4:已完成 5:已关闭 6:售后中 7:退款中 8:订单关闭)
     */
    @Excel(name = "订单状态",replace={"待付款_1","待发货_2","已发货_3","已完成_4","已关闭_5","售后中_6","退款中_7","订单关闭_8"})
    private int status;

    /**
     * 维权状态 (1:未维权 2:维权中 3:维权结束)
     */
    @Excel(name = "维权状态",replace = {"未维权_1","维权中_2","维权结束_3"})
    private int rightsProtectionStatus;

    /**
     * 收货人姓名
     */
    @Excel(name = "收货人姓名")
    private String receiverName;

    /**
     * 收货人手机号
     */
    @Excel(name = "收货人手机号")
    private String receiverPhone;

    /**
     * 收货人手机号后四位
     */
    @ExcelIgnore
    private String receiverPhoneLastFour;

    /**
     * 物流记录id
     */
    @ExcelIgnore
    private Integer shipmentId;

    /**
     * 物流记录编号
     */
    @Excel(name = "物流记录编号")
    private String shipmentNo;

    /**
     * 物流编号
     */
    @Excel(name = "物流编号")
    private String trackingNo;


    /**
     * 支付方式 (1: 支付宝 2: 微信 3:银联 4:其他)
     */
    @Excel(name = "支付方式",replace = {"支付宝_1","微信_2","银联_3","其他_4"})
    private int payMethod;

    /**
     * 订单加星数
     */
    @Excel(name = "订单加星数")
    private String payStarred;

    /**
     * 订单类型 (1: 普通订单 2: 代付订单 3:送礼订单 4:送礼社群版订单 5:心愿订单 6:扫码付款 7:酒店订单 8:维权订单 9:周期购订单 10:多人拼团订单 11:知识付费订单)
     */
    @Excel(name = "订单类型",replace = {"普通订单_1","代付订单_2","送礼订单_3","送礼社群版订单_4","心愿订单_5","扫码付款_6","酒店订单_7","维权订单_8","周期购订单_9","多人拼团订单_10","知识付费订单_11"})
    private int type;

    /**
     * 订单来源 (1: 浏览器 2:支付宝 3:浏览器 4:商家自有app 5:微信小程序 6: 其他)
     */
    @Excel(name = "订单来源",replace = {"浏览器_1","支付宝_2","浏览器_3","商家自有app_4","微信小程序_5","其他_6"})
    private int source;

    /**
     * 配送方式(1:快递发货 2:上门自提 3:同城配送)
     */
    @Excel(name = "配送方式",replace = {"快递发货_1","上门自提_2","同城配送_3"})
    private int deliveryMethod;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间",exportFormat= DateTimeFormatConst.DEFAULT_DATETIME_PATTERN)
    private Date createTime;
    /**
     * 订单详情id
     */
    @ExcelIgnore
    private Integer orderDetailId;
    /**
     * 商品id
     */
    @ExcelIgnore
    private Integer productId;

    /**
     * 商品编号
     */
    @Excel(name = "商品编号")
    private String productNo;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String productName;

    /**
     * 商品数量
     */
    @Excel(name = "商品数量")
    private Integer qty;

    /**
     * 应收金额
     */
    @Excel(name = "应收金额")
    private BigDecimal receivableAmount;

    /**
     * 优惠金额
     */
    @Excel(name = "优惠金额")
    private BigDecimal favorableAmount;

    /**
     * 实收金额
     */
    @Excel(name = "实收金额")
    private BigDecimal paidAmount;

    /**
     * 商品sku信息
     */
    @Excel(name = "商品sku信息")
    private String productSku;


    public void setReceivableAmount(BigDecimal receivableAmount) {
        this.receivableAmount = receivableAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
    }

    public void setFavorableAmount(BigDecimal favorableAmount) {
        this.favorableAmount = favorableAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);

    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);

    }


    public String getPayStarred() {
        if(payStarred.equals("-1"))
            return "未加星";
        return payStarred+"星";
    }
}
