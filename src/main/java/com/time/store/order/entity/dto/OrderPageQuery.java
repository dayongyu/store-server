package com.time.store.order.entity.dto;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.time.store.core.entity.base.BasePageRequest;
import com.time.store.core.entity.base.BaseRequest;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class OrderPageQuery extends BasePageRequest {
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 外部支付编号
     */
    private String externalPayNo;
    /**
     * 收货人姓名
     */
    private String receiverName;
    /**
     * 收货人手机号
     */
    private String receiverPhone;

    /**
     * 收货人手机号后四位
     */
    private String receiverPhoneLastFour;
    /**
     * 下单开始时间
     */
    private LocalDateTime createTimeStart;
    /**
     * 下单结束时间
     */
    private LocalDateTime createTimeEnd;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 订单状态(1:待付款 2:待发货 3:已发货 4:已完成 5:已关闭 6:售后中 7:退款中 8:订单关闭)
     */
    private Integer orderStatus;
    /**
     * 维权状态 (1:未维权 2:维权中 3:维权结束)
     */
    private Integer rightsProtectionStatus;
    /**
     * 订单类型 (1: 普通订单 2: 代付订单 3:送礼订单 4:送礼社群版订单 5:心愿订单 6:扫码付款 7:酒店订单 8:维权订单 9:周期购订单 10:多人拼团订单 11:知识付费订单)
     */
    private Integer type;

    /**
     * 订单来源 (1: 浏览器 2:支付宝 3:浏览器 4:商家自有app 5:微信小程序 6: 其他)
     */
    private Integer source;
    /**
     * 配送方式(1:快递发货 2:上门自提 3:同城配送)
     */
    private Integer deliveryMethod;
    /**
     * 支付方式 (1: 支付宝 2: 微信 3:银联 4:其他)
     */
    private Integer payMethod;
    /**
     * 加星状态(1:不加星 2:加星)
     */
    private Integer payStarredStatus;


}
