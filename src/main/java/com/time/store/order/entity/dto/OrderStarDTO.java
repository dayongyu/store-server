package com.time.store.order.entity.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class OrderStarDTO {
    /**
     * 订单id
     */
    @NotNull
    private Integer orderId;
    /**
     * 订单加星数量
     */
    @Min(0)
    @Max(5)
    private Integer payStarred;
}
