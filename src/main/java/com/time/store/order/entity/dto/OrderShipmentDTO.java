package com.time.store.order.entity.dto;

import lombok.Data;


@Data
public class OrderShipmentDTO {

    /**
     * 订单ID
     */
    private String orderNo;
    /**
     * 物流编号
     */
    private String tackingNo;
    /**
     * 物流名称 中通,申通 等
     */
    private String tackingName;


}
