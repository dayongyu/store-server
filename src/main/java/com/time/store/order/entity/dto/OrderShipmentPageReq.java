package com.time.store.order.entity.dto;

import com.time.store.core.entity.base.BasePageRequest;
import lombok.Data;

@Data
public class OrderShipmentPageReq extends BasePageRequest {

    /**
     *  状态(1: 处理中,2: 已完成)
     */
    private Integer  deliveryStatus;

    /**
     * 操作用户登录名
     */
    private String operatorLoginName;
    /**
     * 创建时间开始时间
     */
    private String createTimeStart;
    /**
     * 创建时间结束时间
     */
    private String createTimeEnd;
}
