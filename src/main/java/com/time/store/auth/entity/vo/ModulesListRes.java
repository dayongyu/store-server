package com.time.store.auth.entity.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.time.store.auth.entity.bo.Modules;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * <p>
 * 模块
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Data
public class ModulesListRes extends Modules {


    /**
     * 是否选中
     */
    private boolean check;
}
