package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class ModulesPageQueryReq {
    /**
     * 模块名称
     */
    private String name;
    /**
     * 模块code
     */
    private String code;

    /**
     * 父模块编号
     */
    private Integer parentId;
    /**
     * 创建时间 开始时间
     */
    private LocalDateTime createTimeStart;
    /**
     * 创建时间 结束时间
     */
    private LocalDateTime createTimeEnd;

}
