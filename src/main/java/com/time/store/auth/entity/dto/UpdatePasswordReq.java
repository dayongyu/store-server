package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class UpdatePasswordReq {



    /**
     * 旧密码
     */
    @NotBlank
    private String password;
    /**
     * 新密码
     */
    @NotBlank
    private String newPassword;
}
