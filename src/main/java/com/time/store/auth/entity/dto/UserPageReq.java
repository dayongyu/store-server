package com.time.store.auth.entity.dto;

import com.time.store.core.entity.base.BasePageRequest;
import lombok.Data;

@Data
public class UserPageReq extends BasePageRequest {
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 是否锁定(0:未锁定 1:已锁定)
     */
    private Integer isLockout;
    /**
     * 密保邮箱
     */
    private String protectEmail;

    /**
     * 密保手机号
     */
    private String protectPhone;
    /**
     * 创建时间开始时间
     */
    private String createTimeStart;
    /**
     * 创建时间结束时间
     */
    private String createTimeEnd;
}
