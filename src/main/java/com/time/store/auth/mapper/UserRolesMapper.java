package com.time.store.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.auth.entity.bo.UserRoles;
import com.time.store.auth.entity.dto.AddUserSetRoleReq;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface UserRolesMapper extends BaseMapper<UserRoles> {

}
