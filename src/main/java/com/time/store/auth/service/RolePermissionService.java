package com.time.store.auth.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.auth.entity.bo.RolePermission;
import com.time.store.auth.mapper.RolePermissionMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class RolePermissionService extends ServiceImpl<RolePermissionMapper, RolePermission> implements IService<RolePermission> {

}
