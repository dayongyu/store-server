package com.time.store.auth.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.auth.entity.bo.UserRoles;
import com.time.store.auth.entity.dto.AddUserSetRoleReq;
import com.time.store.auth.mapper.UserRolesMapper;
import com.time.store.core.util.AuthUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class UserRolesService extends ServiceImpl<UserRolesMapper, UserRoles> implements IService<UserRoles> {


}
