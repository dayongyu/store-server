package com.time.store.auth.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.lang.tree.Tree;
import com.time.store.auth.entity.base.ActiveUser;
import com.time.store.auth.entity.bo.Permission;
import com.time.store.auth.entity.bo.Users;
import com.time.store.auth.entity.dto.LoginImgReq;
import com.time.store.auth.entity.dto.LoginUserReq;
import com.time.store.auth.entity.vo.ImgCode;
import com.time.store.auth.entity.vo.LoginRes;
import com.time.store.auth.service.ModulesService;
import com.time.store.auth.service.PermissionService;
import com.time.store.auth.service.UsersService;
import com.time.store.core.annotations.IgnoreAuth;
import com.time.store.core.annotations.LoginUser;
import com.time.store.core.consts.RedisKeyConst;
import com.time.store.core.util.AssertUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * 登录控制器
 *@description: 登录控制器
 *@menu 登录
 */
@RestController
@RequestMapping("/api/auth/login")
@Slf4j
public class LoginController {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private UsersService usersService;

    /**
     * 获取登录图形验证码
     * @status done
     * @param loginImgReq
     * @return
     */
    @GetMapping("/img")
    @IgnoreAuth
    public ImgCode getImgCode(@Validated LoginImgReq loginImgReq) {
        //定义图形验证码的长和宽
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(loginImgReq.getWidth(), loginImgReq.getHeight());
        String imageBase64 = lineCaptcha.getImageBase64Data();

        String imgId = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set(RedisKeyConst.IMG_CODE + imgId, lineCaptcha.getCode(), RedisKeyConst.IMG_CODE.getDuration());
        log.info("图形验证码:{}", lineCaptcha.getCode());
        ImgCode imgCode = new ImgCode();
        imgCode.setImgBase64(imageBase64);
        imgCode.setImgId(imgId);
        Long expire = redisTemplate.getExpire(RedisKeyConst.IMG_CODE + imgId);
        imgCode.setExpire(expire);
        return imgCode;
    }

    /**
     * 登录
     * @status done
     */
    @PostMapping("")
    @IgnoreAuth
    public LoginRes login(@RequestBody @Validated LoginUserReq loginUserReq) {
        // 万能验证码
        if(!loginUserReq.getImgId().equals("999999999")){
            AssertUtil.isTrue(redisTemplate.hasKey(RedisKeyConst.IMG_CODE + loginUserReq.getImgId()), "验证码过期");
            Object imgCode = redisTemplate.opsForValue().get(RedisKeyConst.IMG_CODE + loginUserReq.getImgId());
            AssertUtil.isTrue(loginUserReq.getImgCode().equals(imgCode), "验证码错误");
        }

        return usersService.login(loginUserReq);
    }

    /**
     * 获取当前用户信息
     * @status done
     * @param activeUser
     * @return
     */
    @GetMapping("/me")
    public ActiveUser me(@LoginUser ActiveUser activeUser) {
        activeUser.getActiveUsers().setPassword(null);
        activeUser.getActiveUsers().setIsLockout(null);
        activeUser.getActiveUsers().setLockTime(null);
        activeUser.getActiveUsers().setPsdWrongTime(null);
        return activeUser;
    }

    /**
     * 刷新token
     * @status done
     */
    @GetMapping("refreshToken")
    @IgnoreAuth
    public LoginRes refreshToken(@RequestParam String refreshToken) {
        return usersService.refreshToken(refreshToken);
    }
}
