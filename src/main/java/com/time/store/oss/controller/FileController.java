package com.time.store.oss.controller;

import com.time.store.oss.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 文件上传
 */
@RequestMapping("/api/oss/file")
@RestController
public class FileController {
    @Autowired
    private FileService fileService;

    /**
     * 图片上传
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping("/imgUpload")
    public String upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        File upload = new File(path.getAbsolutePath(), file.getOriginalFilename());
        file.transferTo(upload);
        return fileService.uploadOss(upload);
    }
}
