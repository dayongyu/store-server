package com.time.store.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.product.entity.bo.ProductEvaluate;
import com.time.store.product.entity.bo.ProductStoreReply;
import com.time.store.product.entity.query.ProductEvaluateQuery;
import com.time.store.product.entity.vo.ProductEvaluatePageVO;
import org.apache.ibatis.annotations.Param;

public interface ProductStoreReplyMapper extends BaseMapper<ProductStoreReply> {




}
