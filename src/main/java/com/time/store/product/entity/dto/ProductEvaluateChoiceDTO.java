package com.time.store.product.entity.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.executable.ValidateOnExecution;

/**
 * 评论加精
 */
@Data
public class ProductEvaluateChoiceDTO {
    /**
     * 评价id
     */
    @NotNull
    private Integer id;

}
