package com.time.store.product.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Data
public class ProductInfoUpdateDTO {

    /**
     * 商品id
     */
    private Integer id;

    /**
     * 商品名称
     */
    @NotEmpty
    private String productName;

    /**
     * 商品分类id
     */
    @NotNull
    private Integer productCategoryId;

    /**
     * 商品价格
     */
    @NotNull
    private Integer price;

    /**
     * 上下架状态：0下架1上架
     */
    @NotNull
    private Integer publishStatus;

    /**
     * 生产日期
     */
    @NotNull
    private Date productionDate;

    /**
     * 商品描述
     */
    @NotNull
    private String description;

    /**
     * 商品图片
     */
    @NotNull
    private String picUrl;

    /**
     * 扩展属性，比如：宽，高，长、宽
     */
    @NotNull
    private String property;

    /**
     * 会员价
     */
    private BigDecimal memberPrice;
}
