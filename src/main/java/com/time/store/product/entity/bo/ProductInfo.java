package com.time.store.product.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("product_info")
@Data
public class ProductInfo extends Model<ProductInfo> {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 商品编码
     */
    private String productNo;

    /**
     * 商品名称
     */
    @NotEmpty
    private String productName;

    /**
     * 商品分类id
     */
    @NotNull
    private Integer productCategoryId;

    /**
     * 商品价格
     */
    @NotNull
    private Integer price;

    /**
     * 上下架状态：0下架1上架
     */
    @NotNull
    private Integer publishStatus;

    /**
     * 生产日期
     */
    @NotNull
    private Date productionDate;

    /**
     * 商品描述
     */
    @NotNull
    private String description;

    /**
     * 商品图片
     */
    @NotNull
    private String picUrl;

    /**
     * 扩展属性，比如：宽，高，长、宽
     */
    @NotNull
    private String property;

    /**
     * 会员价
     */
    private BigDecimal memberPrice;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
