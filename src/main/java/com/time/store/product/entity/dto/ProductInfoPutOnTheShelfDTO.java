package com.time.store.product.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Data
public class ProductInfoPutOnTheShelfDTO {

    /**
     * 商品id
     */
    private Integer id;
    /**
     * 上下架状态：0下架1上架
     */
    @NotNull
    private Integer publishStatus;
}
