package com.time.store.product.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商品分类表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("product_category")
@Data
public class ProductCategory extends Model<ProductCategory> {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 分类编码
     */
    private String categoryCode;

    /**
     * 分类描述
     */
    private String categoryDesc;

    /**
     * 排序
     */
    private Integer categoryOrder;

    /**
     * 父分类id
     */
    private Integer parentId;

    /**
     * 分类层级
     */
    private int categoryLevel;

    /**
     * 分类状态
     */
    private int categoryStatus;

    /**
     * 分类 icon
     */
    private String iconUrl;

    /**
     * 分类图片
     */
    private String picUrl;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
