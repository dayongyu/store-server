package com.time.store;


import com.time.store.auth.entity.bo.Modules;
import com.time.store.auth.entity.bo.Permission;
import com.time.store.auth.entity.bo.RoleModules;
import com.time.store.auth.entity.bo.UserRoles;
import com.time.store.auth.service.*;
import com.time.store.core.util.AuthUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class AuthTests {
    @Autowired
    private RolePermissionService rolePermissionService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RoleModulesService roleModulesService;
    @Autowired
    private UserRolesService userRolesService;
    @Autowired
    private ModulesService modulesService;

    /**
     * 设置管理员所有模块
     */
    @Test
    public void setAdminModule() {
        List<Modules> list = modulesService.list();
        List<RoleModules> roleModules = list.stream().map(item -> {
            RoleModules roleModule = new RoleModules();
            roleModule.setRoleId(1);
            roleModule.setModuleId(item.getId());
            return roleModule;
        }).collect(Collectors.toList());
        roleModulesService.saveBatch(roleModules);
    }

    /**
     * 设置管理员所有模块
     */
    @Test
    public void setModuleCode() {
        AuthUtil.setRequestStoreId(1);
        List<Modules> list = modulesService.list();
        for (Modules modules : list) {
            modules.setCode("code"+modules.getId());
            modulesService.updateById(modules);
        }
    }

}
